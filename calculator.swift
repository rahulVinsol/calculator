import Foundation

class DataStructure<T> {
	var array: [T] = [T]()
	
	var count: Int {
		array.count
	}
	
	func peek() -> T? {
		array.last
	}
	
	func isEmpty() -> Bool {
		array.isEmpty
	}
}

class Stack<T>: DataStructure<T> {
	func pop() -> T? {
		array.popLast()
	}
	
	func push(_ t: T?) {
		if t != nil {
			array.append(t!)
		}
	}
}

class Queue<T>: DataStructure<T> {
	func dequeue() -> T? {
		array.removeFirst()
	}
	
	func enqueue(_ t: T?) {
		if t != nil {
			array.append(t!)
		}
	}
}

enum ExpressionError: Error {
	case invalidToken(token: String)
	case invalidExpression
	case emptyExpression
}

class Calculator {
	private let precedence: Dictionary<String?, Int> = [
		"/": 2,
		"*": 2,
		"+": 1,
		"-": 1,
		"(": 0
	] 
	
	private func tokenize(expression: String) throws -> [String] {
		do{
			let regex = try NSRegularExpression(pattern: "(\\d+|\\D)")
			let results = regex.matches(in: expression, range: NSRange(location: 0, length: expression.count))
			let tokens: Array<String> = results.map{ (checkingResult: NSTextCheckingResult) in NSString(string: expression).substring(with: checkingResult.range).trimmingCharacters(in: .whitespacesAndNewlines) }
			var filteredTokens = [String]()
			for token in tokens {
				switch token {
					case "/", "*", "+", "-", "(", ")": filteredTokens.append(token)
					case let num where Int(num) != nil: filteredTokens.append(token)
					case "": break
					default : throw ExpressionError.invalidToken(token: token)
					
				}
			}
			return filteredTokens
		} catch {
			return []
		}
	}
	
	private func infixToPostfix(_ expression: String) throws -> Queue<String> {
		let tokens = try tokenize(expression: expression)
		if tokens.isEmpty {
			throw ExpressionError.emptyExpression
		}
		
		let queue = Queue<String>()
		let stack = Stack<String>()
		
		func hasLowerPrecedence(_ token: String) -> Bool {
			let stackTopPrecedence = precedence[stack.peek()]
			let tokenPrecedence = precedence[token]
			
			if stackTopPrecedence != nil && tokenPrecedence != nil {
				return stackTopPrecedence! >= tokenPrecedence!
			}
			
			return false
		}
		
		for token in tokens {
			switch token {
				case let num where Int(num) != nil: queue.enqueue(num)
				case "(": stack.push(token)
				case ")": 
					  while stack.peek() != "(" {
					  	queue.enqueue(stack.pop())
					  }
					  _ = stack.pop()
				default:
					  while !stack.isEmpty() && hasLowerPrecedence(token) {
					  	queue.enqueue(stack.pop())
					  }
					  stack.push(token)
			}
		}
		
		while !stack.isEmpty() {
			if stack.peek() == "(" {
				throw ExpressionError.invalidExpression	
			}
			queue.enqueue(stack.pop())
		}
		
		return queue
	}
	
	func calcExpression(expression:String) throws -> Int {
		let postfixQueue = try infixToPostfix(expression)
		
		let resStack = Stack<Int>()
		while !postfixQueue.isEmpty() {
			let token = postfixQueue.dequeue()
			switch token! {
				case let num where Int(num) != nil: resStack.push(Int(num))
				case "+", "-", "/", "*":
					let first = resStack.pop()
					let second = resStack.pop()
					if first != nil && second != nil {
						switch token {
							case "+": resStack.push(second! + first!)
							case "-": resStack.push(second! - first!)
							case "*": resStack.push(second! * first!)
							case "/": resStack.push(second! / first!)
							default: break
						}
					} else {
						throw ExpressionError.invalidExpression
					}
				default: break
			}
		}
		if resStack.count != 1 {
			throw ExpressionError.invalidExpression
		}
		return resStack.pop()!
	}
}


print("Enter expression = ", terminator: "")
let expression = readLine() ?? ""

let calc = Calculator()

do {
	let result = try calc.calcExpression(expression: expression)
	print(result)
} catch ExpressionError.invalidToken(let token) {
	print("Invalid character in the expression \(token)")
} catch ExpressionError.invalidExpression {
	print("Invalid Expression")
} catch ExpressionError.emptyExpression {
	print("Empty Expression")
} catch {
	print("Unknown error")
}
